# texmap.vim
Save time in TeX in Vim.

## Usage
Muh just read the source code.

For example, with this line executed
```
call s:texmap_map(";tc", "\\color{}<left>")
```
, press `;tc` in insert mde will result in `\color{|}` instead (`|` denote
cursor position)

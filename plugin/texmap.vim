" Save time in tex in Vim.
" Maintainer:	Segmentation Fault <segv@disroot.org>
" Last Change: 11/13/2021
" Version: 1.4
" Repository: https://codeberg.org/segv/tex.vim
" License: GPLv3

func! s:texmap_map(from, to)
	let l:mapstr = "autocmd filetype tex inoremap <buffer>"
	exe l:mapstr . a:from . " " . a:to

endf

augroup texmap_maps
	autocmd!
	call s:texmap_map(";d",  "\\documentclass{}<left>")

	" p for preamble, I guess
	call s:texmap_map(";pu",  "\\usepackage{}<left>")
	call s:texmap_map(";pa",  "\\author{}<left>")
	call s:texmap_map(";pd",  "\\date{}<left>")
	call s:texmap_map(";pt",  "\\title{}<left>")

	" s means section
	call s:texmap_map(";ss", "\\section{}<left>")
	call s:texmap_map(";s1", "\\subsection{}<left>")
	call s:texmap_map(";s2", "\\subsubsection{}<left>")

	" section without numbering
	call s:texmap_map(";SS", "\\section*{}<left>")
	call s:texmap_map(";S1", "\\subsection*{}<left>")
	call s:texmap_map(";S2", "\\subsubsection*{}<left>")

	" e means environment
	call s:texmap_map(";eb", "\\begin{}<left>")
	call s:texmap_map(";ee", "\\end{}<left>")
	call s:texmap_map(";ed", "\\begin{document}<cr>##<cr><home>\\end{document}<esc>?##<cr>xxi")
	call s:texmap_map(";el", "\\begin{enumerate}<cr>\\item ##<cr><home>\\end{enumerate}<esc>?##<cr>xxi")
	call s:texmap_map(";eL", "\\begin{itemize}<cr>##<cr><home>\\end{itemize}<esc>?##<cr>xxi")
	call s:texmap_map(";em", "\\begin{equation}<cr>##<cr><home>\\end{equation}<esc>?##<cr>xxi")
	call s:texmap_map(";eM", "\\begin{equation*}<cr>##<cr><home>\\end{equation*}<esc>?##<cr>xxi")
	call s:texmap_map(";ea", "\\begin{align}<cr>##<cr><home>\\end{align}<esc>?##<cr>xxi")
	call s:texmap_map(";eA", "\\begin{align*}<cr>##<cr><home>\\end{align*}<esc>?##<cr>xxi")
	call s:texmap_map(";eg", "\\begin{gather}<cr>##<cr><home>\\end{gather}<esc>?##<cr>xxi")
	call s:texmap_map(";eG", "\\begin{gather*}<cr>##<cr><home>\\end{gather*}<esc>?##<cr>xxi")
	call s:texmap_map(";ev", "\\begin{verbatim}<cr>##<cr><home>\\end{verbatim}<esc>?##<cr>xxi")

	" text formatting
	call s:texmap_map(";te", "\\emph{}<left>")
	call s:texmap_map(";tb", "\\textbf{}<left>")
	call s:texmap_map(";tu", "\\underline{}<left>")
	call s:texmap_map(";ti", "\\textit{}<left>")
	call s:texmap_map(";tc", "\\color{}<left>")
	call s:texmap_map(";ts", "\\textsuperscript{}<left>")
	call s:texmap_map(";tS", "\\textsubscript{}<left>")
	call s:texmap_map(";t^", "\\widehat{}<left>")
	call s:texmap_map(";t~", "\\widetilde{}<left>")
	call s:texmap_map(";t.", "\\dot{}<left>")
	call s:texmap_map(";t2.", "\\ddot{}<left>")


	" label and ref
	call s:texmap_map(";l", "\\label{}<left>")
	call s:texmap_map(";r", "\\ref{}<left>")

	" special character
	call s:texmap_map(";\\", "\\textbackslash{} ")
	call s:texmap_map(";<", "\\textless{} ")
	call s:texmap_map(";>", "\\textgreater{} ")
	call s:texmap_map(";D", "\\textdegree{} ")
	call s:texmap_map(";P", "\\P{} ")
	call s:texmap_map(";T", "\\texttrademark{} ")
	call s:texmap_map(";R", "\\textregisterd{} ")
	call s:texmap_map(";~", "\\textasciitilde{} ")

	call s:texmap_map(";mt", "\\maketitle{}<cr>")
	call s:texmap_map(";np", "\\newpage{}<cr>")


augroup end
